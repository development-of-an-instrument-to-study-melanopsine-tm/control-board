""" This is a docstring line """

import time
import json
import csv
import serial

ENCODING = 'utf-8'

# configure the serial connections
SER = serial.Serial(
    port='/dev/ttyACM1',
    baudrate=9600,
    parity='N',
    stopbits=1,
    bytesize=8,
    timeout=0.1
)

if SER.isOpen():
    print("Serial connection opened withe the STM32")

# ==============================================
# Idividual LED control
# ==============================================
# Read the JSON file
with open('test.json') as f:
    # open the  JSON file as a dictionary
    DATA = json.load(f)
    # dummy first JSON (does nothing)
    SER.write(bytes(json.dumps(DATA).encode()))


# ------------------------------------------
    # Set the base JSON valus for one LED
# ------------------------------------------

    #PERCENT = 0.001
    #DATA['LED']['Duty'] = int(65535 * PERCENT)
    #DATA['LED']['Duty'] = 101
    SER.write(bytes(json.dumps(DATA).encode()))
    #time.sleep(0.5)
    #DATA['LED']['Duty'] = 0
    #SER.write(bytes(json.dumps(DATA).encode()))

# ------------------------------------------
    # set the duty cycle for all LEDs
    # on 16 bits (from 0 to
    # but test them one at the time
# ------------------------------------------
#    for t in range(4):
#        time.sleep(0.5)
#
#        # Apply the new duty cicle to all the LEDs
#        for i in range(16):
#            DATA['LED']['LedIndex'] = i
#            DATA['LED']['Duty'] = 65535
#            SER.write(bytes(json.dumps(DATA).encode()))
#            time.sleep(0.02)
#            DATA['LED']['Duty'] = 0
#            SER.write(bytes(json.dumps(DATA).encode()))
#            time.sleep(0.02)

# ------------------------------------------
    # set the duty cycle for all LEDs
    # on 16 bits (from 0 to 65535)
    # all at once
# ------------------------------------------
#    PERCENT = 0.1
#    ONOFF = False
#
#    for t in range(4):
#        time.sleep(0.5)
#
#        ONOFF = not ONOFF
#
#        DATA['LED']['Duty'] = int(65535 * PERCENT * ONOFF)
#
#        # Apply the new duty cicle to all the LEDs
#        for i in range(16):
#            time.sleep(0.002)
#            DATA['LED']['LedIndex'] = i
#            print(DATA)
#            # Send the JSON encoded in UFT-8
#            SER.write(bytes(json.dumps(DATA).encode()))

# ------------------------------------------
# Read the return on the serial from the STM32
# print(str(SER.read(1000), ENCODING))


# ==============================================
# Patterns test
# ==============================================
# Read the JSON file
#with open('patterns_test.json') as f:
#    # open the  JSON file as a dictionary
#    DATA = json.load(f)
#
#    # Opens the CSV file containing the curve
#    with open('sin_sequence.csv') as csv_file:
#        # Read the lines as an array
#        for SEQ in csv.reader(csv_file, delimiter=','):
#
#            # Convert that array in a list of integer
#            # and pass it the the dictionary
#            DATA['PatternDatas'][0]['Data'] = list(map(int, SEQ))
#            print(json.dumps(DATA))
#            # send the  JSON
#            # This is tricky: the sequence sent is "stored"
#            # on the control board. It is identify by its LedIndex.
#            # We need to send a "StartSequence" command
#            # to lanch the sequence.
#            SER.write(bytes(json.dumps(DATA).encode()))
#            time.sleep(0.2)
#
## Say to the controler to start the sequence
#with open('start.json') as f:
#    # open the  JSON file as a dictionary
#    DATA = json.load(f)
#    SER.write(bytes(json.dumps(DATA).encode()))
#
#
#print(str(SER.read(10000), ENCODING))

# Closes the seril communication
SER.close()
